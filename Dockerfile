FROM node:alpine

RUN npm i -g vscode-langservers-extracted

# CMD can be any of:
# vscode-html-language-server
# vscode-css-language-server
# vscode-json-language-server
# vscode-eslint-language-server
CMD ["vscode-json-language-server"]
